# CInput
A custom Input Manager for Unity I'm working on

## **TODO**

#### General
	Normalize analog input

#### GUI
	Config
		Keyboard binding
		Schemes
		Bindings
		Settings
	
#### Unity tools
	Context menu
		Add CInput obj to scene (make dontdestroy?)
	Menu Items
		Access config GUI
		Add CInput obj to scene (make dontdestroy?)
		Change InputManager
		
#### Data driven (save/load)
	Keyboard bindings
	Schemes
	Mappings
	Input manager file?
	Settings?
	
#### CInput inspector
	Group options
	
#### Optimiziations
	String processing requirement
	