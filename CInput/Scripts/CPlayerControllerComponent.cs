﻿using System;
using UnityEngine;

namespace CInput
{
    /// <summary>
    /// Used to attach a CPlayerController to a player
    /// </summary>
    public class CPlayerControllerComponent : MonoBehaviour
    {
        public CPlayerController mController;
    }

    /// <summary>
    /// Main entry point for parsed input to a specific player
    /// </summary>
    public class CPlayerController
    {

        public CActionMap mActions { get; private set; }
        public ControllerType mCType { get; private set; }
        public JoystickNum mJNum { get; private set; }

        

        private bool mIsActive = true;
        private bool mIsInit = false;

        /// <summary>
        /// Constructor
        /// </summary>
        public CPlayerController()
        {
            mIsInit = false;
            init();
        }

        /// <summary>
        /// Initialize the CPlayerController with type and number
        /// </summary>
        /// <param name="cType">Controller type</param>
        /// <param name="jNum">Joystick number</param>
        public void initController(ControllerType cType, JoystickNum jNum, float axisToButtonActivate, float axisToButtonRelease)
        {
            if (!mIsInit)
                init();

            mCType = cType;
            mJNum = jNum;
            mActions.setAxisToButtonVals(axisToButtonActivate, axisToButtonRelease);
        }

        /// <summary>
        /// Toggle whether controller will actively pass input or not
        /// </summary>
        public void toggleActive()
        {
            if (!mIsInit)
                init();

            mIsActive = !mIsActive;
        }

        /// <summary>
        /// Get whether controller is passing input or not
        /// </summary>
        public bool isActive()
        {
            return mIsActive;
        }



        /// <summary>
        /// Initialize new CPlayerController as unknown
        /// </summary>
        public void init()
        {
            if (!mIsInit)
            {
                mActions = new CActionMap();
                mCType = ControllerType.UNKNOWN_CONTROLLER;
                mJNum = JoystickNum.UNKNOWN_JNUM;
                mIsInit = true;
            }
        }

        /// <summary>
        /// Used to activate a function bound to a button
        /// </summary>
        /// <param name="button">Controller button</param>
        /// <param name="state">Button state (OnDown, OnUp, IsDown</param>
        public void activateButtonFunc(ControllerButton button, KeyState state)
        {
            if (!mIsInit)
                init();

            if (mIsActive)
                mActions.activateButtonFunc(button, state);
        }

        /// <summary>
        /// Used to activate function bound to an axis
        /// </summary>
        /// <param name="axis">Controller axis</param>
        /// <param name="val">Axis value</param>
        public void activateAxisFunc(ControllerAxis axis, float val)
        {
            if (!mIsInit)
                init();

            if (mIsActive)
                mActions.activateAxisFunc(axis, val);
        }

        /// <summary>
        /// Bind a function to a button
        /// </summary>
        /// <param name="button">Controller button to bind</param>
        /// <param name="buttonState">Button state to bind</param>
        /// <param name="func">The action (no parameters allowed)</param>
        public void bindButton(ControllerButton button, KeyState buttonState, Action func)
        {
            if (!mIsInit)
                init();

            mActions.bindButtonForced(button, buttonState, func);
        }

        public void unbindButton(ControllerButton button)
        {
            mActions.unbindButton(button);
        }

        /// <summary>
        /// Bind a function to an axis
        /// </summary>
        /// <param name="axis">Controller axis</param>
        /// <param name="func">The action (with the axis value as parameter)</param>
        public void bindAxis(ControllerAxis axis, Action<float> func)
        {
            if (!mIsInit)
                init();

            mActions.bindAxisForced(axis, func);
        }

        /// <summary>
        /// Bind a function to an axis as button
        /// </summary>
        /// <param name="axis">Controller axis</param>
        /// <param name="func">The action without any parameters</param>
        public void bindAxisAsButton(ControllerAxis axis, KeyState state, Action func)
        {
            if (!mIsInit)
                init();

            mActions.bindAxisAsButtonForced(axis, state,  func);
        }

        /// <summary>
        /// Checks if there are any bound AxisAsButton with OnUp
        /// </summary>
        public bool hasOnUpAxis()
        {
            return mActions.hasOnUpAxis();
        }

        /// <summary>
        /// Checks if an AxisAsButton has OnUp
        /// </summary>
        /// <param name="axis">Controller axis</param>
        public bool isOnUpAxis(ControllerAxis axis)
        {
            return mActions.isOnUpAxis(axis);
        }

        /// <summary>
        /// Unbind all currently bound actions
        /// </summary>
        public void unbindAll()
        {
            if (!mIsInit)
                init();
            mActions.unbindAll();
        }

        
    }
}