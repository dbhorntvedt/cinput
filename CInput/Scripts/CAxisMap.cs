﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CInput
{
    /// <summary>
    /// All controller axes (standardized to XBox axes)
    /// </summary>
    public enum ControllerAxis
    {
        INVALID_AXIS = -1,
        Left_Stick_X,
        Left_Stick_Y,
        Right_Stick_X,
        Right_Stick_Y,
        D_Pad_X,
        D_Pad_Y,
        Triggers,
        Left_Trigger,
        Right_Trigger,
        NUM_AXES
    }

    public enum ControllerAxisMode
    {
        INVALID_MODE = -1,
        Analog,
        Digital,
        NUM_MODES
    }

    /// <summary>
    /// Axis action for CInput
    /// </summary>
    public class AxisAction
    {
        private ControllerAxis mAxis { get; set; }
        private Action<float> mAxisAction { get; set; }

        /// <summary>
        /// AxisAction constructor
        /// </summary>
        /// <param name="axis">Controller axis</param>
        /// <param name="mode">Axis mode</param>
        /// <param name="func">Function to pass float val</param>
        public AxisAction(ControllerAxis axis, Action<float> func)
        {
            mAxis = axis;
            mAxisAction = func;
        }

        /// <summary>
        /// Activate/invoke function
        /// </summary>
        /// <param name="val">Analog input value</param>
        public void activate(float val)
        {
            mAxisAction.DynamicInvoke(val);
        }
    }

    //TODO: add seperate actions on negative/positive!!
    public class AxisAsButtonAction
    {
        private ControllerAxis mAxis { get; set; }
        private bool mIsActive { get; set; }

        private Dictionary<KeyState, Action> mDic = new Dictionary<KeyState, Action>();

        /// <summary>
        /// AxisAction constructor
        /// </summary>
        /// <param name="axis">Controller axis</param>
        /// <param name="mode">Axis mode</param>
        /// <param name="func">Function to pass float val</param>
        public AxisAsButtonAction(ControllerAxis axis, KeyState state, Action func)
        {
            mAxis = axis;
            mDic.Add(state, func);
            mIsActive = false;
        }

        public bool addAction(KeyState state, Action func)
        {
            if (!mDic.ContainsKey(state))
            {
                mDic.Add(state, func);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Rebind an action on button with specified state
        /// </summary>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public void rebindAction(KeyState state, Action func)
        {
            mDic[state] = func;
        }

        /// <summary>
        /// Unbind an action on button with specified state
        /// </summary>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        public void unbindState(KeyState state)
        {
            mDic.Remove(state);
        }

        /// <summary>
        /// Activate/invoke function and handle key states
        /// </summary>
        /// <param name="val">Analog input value</param>
        /// <param name="activate">Key activate value</param>
        /// <param name="release">Key release value</param>
        public void activate(float val, float activate, float release)
        {
            if (!mIsActive && (val >= activate || val <= -activate))
            {
                mIsActive = true;
                if (mDic.ContainsKey(KeyState.OnDown)) mDic[KeyState.OnDown].Invoke();

                //Debug.Log("On down");

            }
            else if (mIsActive && (val <= release && val >= -release))
            {
                mIsActive = false;
                if (mDic.ContainsKey(KeyState.OnUp)) mDic[KeyState.OnUp].Invoke();

                //Debug.Log("On up");

            }
            else if (val >= activate || val <= -activate)
                if (mDic.ContainsKey(KeyState.IsDown)) mDic[KeyState.IsDown].Invoke();
        }

        public bool hasOnUp() { return mDic.ContainsKey(KeyState.OnUp); }
    }

    /// <summary>
    /// Axis mappings and parsing
    /// </summary>
    public class AxisMap
    {
        public ControllerAxis parseAxisInput(string axis, ControllerType type)
        {
            switch (type)
            {
                case ControllerType.PlayStation3:
                    if (PS3AxisMap.ContainsKey(axis))
                        return PS3AxisMap[axis];
                    else
                        return ControllerAxis.INVALID_AXIS;
                case ControllerType.PlayStation4:
                    if (PS4AxisMap.ContainsKey(axis))
                        return PS4AxisMap[axis];
                    else
                        return ControllerAxis.INVALID_AXIS;
                case ControllerType.XBox360:
                    if (XBox360AxisMap.ContainsKey(axis))
                        return XBox360AxisMap[axis];
                    else
                        return ControllerAxis.INVALID_AXIS;
                case ControllerType.XBox1:
                    if (XBoxOneAxisMap.ContainsKey(axis))
                        return XBoxOneAxisMap[axis];
                    else
                        return ControllerAxis.INVALID_AXIS;
                default:
                    return ControllerAxis.INVALID_AXIS;

            }
        }

        private readonly Dictionary<string, ControllerAxis> PS3AxisMap = new Dictionary<string, ControllerAxis>()
        {
            {"axis y", ControllerAxis.Left_Stick_X},
            {"axis x", ControllerAxis.Left_Stick_Y},
            {"axis 3", ControllerAxis.Right_Stick_X},
            {"axis 5", ControllerAxis.Right_Stick_Y},
            {"axis 6", ControllerAxis.D_Pad_X},
            {"axis 7", ControllerAxis.D_Pad_Y},
            {"axis 9", ControllerAxis.Left_Trigger},
            {"axis 10",ControllerAxis.Right_Trigger},
        };

        private readonly Dictionary<string, ControllerAxis> PS4AxisMap = new Dictionary<string, ControllerAxis>()
        {
            {"axis y", ControllerAxis.Left_Stick_X},
            {"axis x", ControllerAxis.Left_Stick_Y},
            {"axis 3", ControllerAxis.Right_Stick_X},
            {"axis 6", ControllerAxis.Right_Stick_Y},
            {"axis 7", ControllerAxis.D_Pad_X},
            {"axis 8", ControllerAxis.D_Pad_Y},
            {"axis 4", ControllerAxis.Left_Trigger},
            {"axis 5", ControllerAxis.Right_Trigger},
        };

        private readonly Dictionary<string, ControllerAxis> XBox360AxisMap = new Dictionary<string, ControllerAxis>()
        {
            { "axis x",  ControllerAxis.Left_Stick_X},
            { "axis y",  ControllerAxis.Left_Stick_Y},
            { "axis 4",  ControllerAxis.Right_Stick_X},
            { "axis 5",  ControllerAxis.Right_Stick_Y},
            { "axis 6",  ControllerAxis.D_Pad_X},
            { "axis 7",  ControllerAxis.D_Pad_Y},
            { "axis 3",  ControllerAxis.Triggers},
            { "axis 9",  ControllerAxis.Left_Trigger},
            { "axis 10", ControllerAxis.Right_Trigger},
        };

        private readonly Dictionary<string, ControllerAxis> XBoxOneAxisMap = new Dictionary<string, ControllerAxis>()
        {
            { "axis x",  ControllerAxis.Left_Stick_X},
            { "axis y",  ControllerAxis.Left_Stick_Y},
            { "axis 4",  ControllerAxis.Right_Stick_X},
            { "axis 5",  ControllerAxis.Right_Stick_Y},
            { "axis 7",  ControllerAxis.D_Pad_X},
            { "axis 8",  ControllerAxis.D_Pad_Y},
            { "axis 3",  ControllerAxis.Triggers},
            { "axis 9",  ControllerAxis.Left_Trigger},
            { "axis 10",  ControllerAxis.Right_Trigger},
        };
    }
}