﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CInput
{
    /// <summary>
    /// All possible controller axes with both a positive and negative entry
    /// </summary>
    public enum KBtoControllerAxis
    {
        INVALID_KB_AXIS = -1,
        Left_Stick_X_Pos,
        Left_Stick_Y_Pos,
        Right_Stick_X_Pos,
        Right_Stick_Y_Pos,
        D_Pad_X_Pos,
        D_Pad_Y_Pos,
        Triggers_Pos,
        Left_Trigger_Pos,
        Right_Trigger_Pos,
        Left_Stick_X_Neg,
        Left_Stick_Y_Neg,
        Right_Stick_X_Neg,
        Right_Stick_Y_Neg,
        D_Pad_X_Neg,
        D_Pad_Y_Neg,
        Triggers_Neg,
        NUM_KB_AXES
    }

    /// <summary>
    /// Used to pass axes with a value
    /// </summary>
    public struct AxisValPair
    {
        public ControllerAxis mAxis;
        public float mVal;

        public AxisValPair(ControllerAxis axis, float val)
        {
            mAxis = axis;
            mVal = val;
        }
    }

    public class KBtoControllerMap
    {
        private float mKeyToAxisVal;

        public KBtoControllerMap(float keyToAxisVal)
        {
            mKeyToAxisVal = keyToAxisVal;
        }

        /// <summary>
        /// Parse keyboard input to controller axis input
        /// </summary>
        /// <param name="key">Keyboard KeyCode</param>
        /// <returns></returns>
        public AxisValPair parseKBtoAxis(KeyCode key)
        {
            KBtoControllerAxis axis = KBtoAxisMap[key];

            if ((int) axis < 9)
                return new AxisValPair((ControllerAxis) axis, mKeyToAxisVal);
            else if ((int) axis < (int) KBtoControllerAxis.NUM_KB_AXES)
                return new AxisValPair((ControllerAxis) (axis - 9), -mKeyToAxisVal);
            else
                return new AxisValPair(ControllerAxis.INVALID_AXIS, 0);
        }

        /// <summary>
        /// Parse keyboard input to controller button input
        /// </summary>
        /// <param name="key">Keyboard KeyCode</param>
        /// <returns></returns>
        public ControllerButton parseKBtoButton(KeyCode key)
        {
            return KBtoButtonMap[key];
        }

        /// <summary>
        /// Parse mouse input to controller axis input
        /// </summary>
        /// <param name="mouseAxis">Mouse axis string</param>
        /// <param name="val">Value of mouse input</param>
        /// <returns></returns>
        //public AxisValPair parseMousetoAxis(string mouseAxis, float val)
        //{
        //    return new AxisValPair(MouseToAxisMap[mouseAxis], val);
        //}

        public readonly Dictionary<KeyCode, KBtoControllerAxis> KBtoAxisMap = new Dictionary<KeyCode, KBtoControllerAxis>()
        {
            {KeyCode.W, KBtoControllerAxis.Left_Stick_Y_Pos},
            {KeyCode.S, KBtoControllerAxis.Left_Stick_Y_Neg},
            {KeyCode.D, KBtoControllerAxis.Left_Stick_X_Pos},
            {KeyCode.A, KBtoControllerAxis.Left_Stick_X_Neg},
            {KeyCode.Mouse1, KBtoControllerAxis.Left_Trigger_Pos},
            {KeyCode.Mouse0, KBtoControllerAxis.Right_Trigger_Pos},
            {KeyCode.Alpha1, KBtoControllerAxis.D_Pad_Y_Pos },
            {KeyCode.Alpha2, KBtoControllerAxis.D_Pad_X_Pos },
            {KeyCode.Alpha3, KBtoControllerAxis.D_Pad_Y_Neg },
            {KeyCode.Alpha4, KBtoControllerAxis.D_Pad_X_Neg },
        };

        public readonly Dictionary<KeyCode, ControllerButton> KBtoButtonMap = new Dictionary<KeyCode, ControllerButton>()
        {
            {KeyCode.Space, ControllerButton.A},
            {KeyCode.LeftShift, ControllerButton.Left_Stick_Click},
            //{KeyCode.F, ControllerButton.X},
            //{KeyCode.LeftControl, ControllerButton.B},
            {KeyCode.Tab, ControllerButton.Back},
            {KeyCode.Return, ControllerButton.Start},
            {KeyCode.R, ControllerButton.Y},
            {KeyCode.Q, ControllerButton.Left_Bumper},
            //{KeyCode.E, ControllerButton.Right_Bumper},    //TEMPORARY! Switch back
            //{KeyCode.Mouse0, ControllerButton.B}, //TEMPORARY! Switch back
            //{KeyCode.Mouse1, ControllerButton.X}, //TEMPORARY! Switch back
            {KeyCode.E, ControllerButton.Right_Bumper}, //TEMPORARY! Switch back

        };

        public readonly Dictionary<string, ControllerAxis> MouseToAxisMap = new Dictionary<string, ControllerAxis>()
        {
            {"mouse X", ControllerAxis.Right_Stick_X},
            {"mouse Y", ControllerAxis.Right_Stick_Y},
            //{"mouse ScrollWheel", ControllerAxis.Right_Stick_Y},
        };
    }
}
