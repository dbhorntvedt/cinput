﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CInput
{
    /// <summary>
    /// Current valid controller types
    /// </summary>
    public enum ControllerType
    {
        UNKNOWN_CONTROLLER = -1,
        //Windows = 0,
        //MotioninJoy,
        Keyboard_Mouse,
        PlayStation3,
        PlayStation4,
        XBox360,
        XBox1,
        //Steam,
        NUM_CONTROLLER_TYPES
    }

    /// <summary>
    /// Valid joystick number. KB (0) is keyboard/mouse
    /// </summary>
    public enum JoystickNum
    {
        UNKNOWN_JNUM = -1,
        KB,
        J1,
        J2,
        J3,
        J4,
        J5,
        J6,
        J7,
        J8,
        J9,
        J10,
        J11
    }

    /// <summary>
    /// Whether a button is being held down, was just pressed or just released
    /// </summary>
    public enum KeyState
    {
        IsDown,
        OnDown,
        OnUp
    }


    /// <summary>
    /// Controller input manager. Only one is needed in any scene
    /// </summary>
    public class CManager : MonoBehaviour
    {
        /// <summary>
        /// Max size of Unity GetJoystickName array + Keyboard/mouse
        /// </summary>
        public const int MAX_CONTROLLERS = 12;

        // Analog modifiers
        [SerializeField] private float mMouseXSensitivity = 1.0f;
        [SerializeField] private bool mInvertMouseX = false;

        [SerializeField] private float mMouseYSensitivity = 1.0f;
        [SerializeField] private bool mInvertMouseY = false;

        [SerializeField] private float mLeftStickXSensitivity = 1.0f;
        [SerializeField] private bool mInvertLeftStickX = false;

        [SerializeField] private float mLeftStickYSensitivity = 1.0f;
        [SerializeField] private bool mInvertLeftStickY = true;      //Seems inverted by default for some reason. Leaving this true will make it positive

        [SerializeField] private float mRightStickXSensitivity = 1.0f;
        [SerializeField] private bool mInvertRightStickX = false;

        [SerializeField] private float mRightStickYSensitivity = 1.0f;
        [SerializeField] private bool mInvertRightStickY = true;      //Seems inverted by default for some reason. Leaving this true will make it positive

        // Value for press and release axis as button
        // For xbox controller values go from -0.375 to 0.375.. why??
        [SerializeField] private float mAxisToButtonActivate = 0.3f;
        [SerializeField] private float mAxisToButtonRelease = 0.2f;

        [SerializeField] private float mButtonToAxisVal = 0.375f;

        private CPlayerController[] mPlayerControllers;

        private ButtonMap mButtonMap;
        private AxisMap mAxisMap;
        private KBtoControllerMap mKBtoControllerMap;

        // Used when mapping controllers to players
        private bool mLookingForControllers = false;
        public bool IsLookingForControllers { get { return mLookingForControllers; } }
        private List<JoystickNum> mLookForExceptions;
        private Action<CPlayerController> mLookForContAct;

        private bool mIsInit = false;

        /// <summary>
        /// Joystick type string to enum
        /// </summary>
        public readonly Dictionary<string, ControllerType> ControllerIdentifier = new Dictionary<string, ControllerType>()
        {
            //{ "MotionJoy Virtual Game Controller"     ,ControllerType.MotioninJoy },
            { "PLAYSTATION(R)3 Controller"            , ControllerType.PlayStation3},
            { "ps4"                                   , ControllerType.PlayStation4},
            { "Controller (Xbox 360 Wireless Receiver for Windows)", ControllerType.XBox360     },
            { "Controller (XBOX 360 For Windows)",      ControllerType.XBox360     },
            { "Controller (Xbox One For Windows)",      ControllerType.XBox1       },
            { "kbm",                                     ControllerType.Keyboard_Mouse }
            //{ "steam"                                 ,ControllerType.Steam       },
        };

        /// <summary>
        /// Initialization
        /// </summary>
        void Start()
        {
            if (!mIsInit)
            {
                mPlayerControllers = new CPlayerController[MAX_CONTROLLERS];
                for (int i = 0; i < MAX_CONTROLLERS; i++)
                    mPlayerControllers[i] = null;

                mButtonMap = new ButtonMap();
                mAxisMap = new AxisMap();
                mKBtoControllerMap = new KBtoControllerMap(mButtonToAxisVal);

                mIsInit = true;
            }
        }

        /// <summary>
        /// It's Update. What do you want from me??
        /// </summary>
        void Update()
        {
            getAllControllerInput();
            getKBMInput();
        }

        /// <summary>
        /// Get value for activation of axis as button
        /// </summary>
        public float getAxisToButtonActivate() { return mAxisToButtonActivate; }

        /// <summary>
        /// Get value for release of axis as button
        /// </summary>
        public float getAxisToButtonRelease() { return mAxisToButtonRelease; }

        /// <summary>
        /// Start the routine for mapping controllers to players
        /// </summary>
        /// <param name="addPlayerFunc">Function to call when input from an unbound controller has been detected</param>
        /// <param name="exceptions">Joysticks to not include in routine (will not be bound)</param>
        public void startLookForControllers(Action<CPlayerController> addPlayerFunc, params JoystickNum[] exceptions)
        {
            if (!mIsInit)
                Start();

            mLookingForControllers = true;
            mLookForContAct = addPlayerFunc;

            mLookForExceptions = new List<JoystickNum>();

            //Change exceptions array to a list
            foreach (JoystickNum num in exceptions)
                mLookForExceptions.Add(num);

            //Populate mPlayerControllers array with new controllers for detection
            for (int i = 0; i < MAX_CONTROLLERS; i++)
                if (!mLookForExceptions.Contains((JoystickNum)i))
                    addPlayerController(new CPlayerController(), (JoystickNum)i);
        }

        /// <summary>
        /// Stop mapping routine
        /// </summary>
        public void stopLookingForControllers()
        {
            if (!mLookingForControllers)
                return;

            mLookingForControllers = false;

            //Set unused controllers to null
            for (int i = 0; i < MAX_CONTROLLERS; i++)
            {
                if (mPlayerControllers[i] != null && mPlayerControllers[i].mCType == ControllerType.UNKNOWN_CONTROLLER)
                    mPlayerControllers[i] = null;
            }

            mLookForExceptions.Clear();
        }

        /// <summary>
        /// Function to handle input from unbound controllers
        /// </summary>
        /// <param name="jNum">Joystick num</param>
        private void handleLookForCont(JoystickNum jNum)
        {
            //Initialize controller
            initializePlayerController(jNum);

            //Add controller to exceptions list
            mLookForExceptions.Add(jNum);

            //Call function for handling new player
            mLookForContAct(mPlayerControllers[(int)jNum]);
        }

        /// <summary>
        /// Initialize a player controller
        /// </summary>
        /// <param name="jNum">JoystickNum</param>
        public void initializePlayerController(JoystickNum jNum)
        {
            if (!mIsInit)
                Start();

            mPlayerControllers[(int)jNum].initController(ControllerIdentifier[getJoystickName(jNum)], jNum, mAxisToButtonActivate, mAxisToButtonRelease);
        }

        /// <summary>
        /// Add a new player controller manually from outside CInput
        /// </summary>
        /// <param name="controller">CPlayerController</param>
        /// <param name="jNum">Joystick num</param>
        public void addPlayerController(CPlayerController controller, JoystickNum jNum)
        {
            if (!mIsInit)
                Start();

            mPlayerControllers[(int)jNum] = controller;
        }

        /// <summary>
        /// Convert raw button input to known button
        /// </summary>
        /// <param name="jNum">Joystick number</param>
        /// <param name="button">Button press (unparsed)</param>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        private void handleButtonInput(JoystickNum jNum, string button, KeyState state)
        {
            if (mPlayerControllers[(int)jNum] != null)
                handleButtonInput(jNum, mButtonMap.parseButtonInput(button, mPlayerControllers[(int)jNum].mCType), state);
        }

        /// <summary>
        /// Handle parsed button input
        /// </summary>
        /// <param name="jNum">Joystick number</param>
        /// <param name="button">Button press (parsed)</param>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        private void handleButtonInput(JoystickNum jNum, ControllerButton button, KeyState state)
        {
            //Bind a new controller
            if (mLookingForControllers && !mLookForExceptions.Contains(jNum) && state == KeyState.OnDown)
                handleLookForCont(jNum);

            //Handle input from controller that is already mapped
            else if (mPlayerControllers[(int)jNum] != null && mPlayerControllers[(int)jNum].mCType != ControllerType.UNKNOWN_CONTROLLER)
                mPlayerControllers[(int)jNum].activateButtonFunc(button, state);
        }

        /// <summary>
        /// How to handle raw axis input
        /// </summary>
        /// <param name="jNum">Joystick number</param>
        /// <param name="axis">Axis (unparsed)</param>
        /// <param name="val">Axis value (-1f - 1f)</param>
        private void handleAxisInput(JoystickNum jNum, string axis, float val)
        {
            handleAxisInput(jNum, mAxisMap.parseAxisInput(axis, mPlayerControllers[(int)jNum].mCType), val);
        }

        /// <summary>
        /// How to handle parsed axis input
        /// </summary>
        /// <param name="jNum">Joystick number</param>
        /// <param name="axis">Axis</param>
        /// <param name="val">Axis value (-1f to 1f)</param>
        private void handleAxisInput(JoystickNum jNum, ControllerAxis axis, float val)
        {
            //Apply modifiers to axes
            if (jNum != JoystickNum.KB)
            {
                switch (axis)
                {
                    case ControllerAxis.Left_Stick_X:
                        val *= mLeftStickXSensitivity;
                        val *= (mInvertLeftStickX) ? -1 : 1;    //Multiply val with -1 if should be inverted
                        break;
                    case ControllerAxis.Left_Stick_Y:
                        val *= mLeftStickYSensitivity;
                        val *= (mInvertLeftStickY) ? -1 : 1;    //Multiply val with -1 if should be inverted
                        break;
                    case ControllerAxis.Right_Stick_X:
                        val *= mRightStickXSensitivity;
                        val *= (mInvertRightStickX) ? -1 : 1;    //Multiply val with -1 if should be inverted
                        break;
                    case ControllerAxis.Right_Stick_Y:
                        val *= mRightStickYSensitivity;
                        val *= (mInvertRightStickY) ? -1 : 1;    //Multiply val with -1 if should be inverted
                        break;
                }
            }

            //Handle input from already bound controller
            if (mPlayerControllers[(int)jNum] != null && mPlayerControllers[(int)jNum].mCType != ControllerType.UNKNOWN_CONTROLLER)
                //Handling AxisAsButton with OnUp while not wasting calls to axes that don't need it
                if ((val == 0f && mPlayerControllers[(int)jNum].isOnUpAxis(axis)) || val != 0f)
                    mPlayerControllers[(int)jNum].activateAxisFunc(axis, val);
        }

        /// <summary>
        /// Get the type string for a given joystick
        /// </summary>
        /// <param name="jNum">Joystick Number</param>
        /// <returns></returns>
        public string getJoystickName(JoystickNum jNum)
        {
            if (jNum == JoystickNum.KB)
                return "kbm";

            return Input.GetJoystickNames()[(int)jNum - 1]; //Array is 1 indexed
        }

        /// <summary>
        /// Get length of Unity's Joystick name array
        /// </summary>
        /// <returns></returns>
        public int getJoystickNameArrLength()
        {
            return Input.GetJoystickNames().Length;
        }


        /// <summary>
        /// Get input only from active player input (controllers bound to players currently in game)
        /// </summary>
        void getBoundControllerInput()
        {

        }

        /// <summary>
        /// Poll keyboard and mouse input
        /// </summary>
        void getKBMInput()
        {
            if (!mIsInit)
                Start();

            //Handle input from keyboard bound as axes
            foreach (KeyValuePair<KeyCode, KBtoControllerAxis> pair in mKBtoControllerMap.KBtoAxisMap)
            {
                AxisValPair axisVal = mKBtoControllerMap.parseKBtoAxis(pair.Key);

                if (Input.GetKey(pair.Key))
                    handleAxisInput(0, axisVal.mAxis, axisVal.mVal);
                else if(mPlayerControllers[(int)JoystickNum.KB].isOnUpAxis(axisVal.mAxis))
                    handleAxisInput(0, axisVal.mAxis, 0.0f);
            }

            //Handle input from keyboard bound as buttons
            foreach (KeyValuePair<KeyCode, ControllerButton> pair in mKBtoControllerMap.KBtoButtonMap)
            {
                if (Input.GetKeyDown(pair.Key))
                    handleButtonInput(0, pair.Value, KeyState.OnDown);

                if (Input.GetKeyUp(pair.Key))
                    handleButtonInput(0, pair.Value, KeyState.OnUp);

                if (Input.GetKey(pair.Key))
                    handleButtonInput(0, pair.Value, KeyState.IsDown);
            }

            //Handle input from mouse
            foreach (KeyValuePair<string, ControllerAxis> pair in mKBtoControllerMap.MouseToAxisMap)
            {
                float val = Input.GetAxis(pair.Key);
                if (val != 0.0f)
                {
                    //Apply sensitivity/inversion
                    if (pair.Key == "mouse X")
                    {
                        val *= mMouseXSensitivity;
                        val *= (mInvertMouseX) ? -1 : 1;
                    }
                    else if (pair.Key == "mouse Y")
                    {
                        val *= mMouseYSensitivity;
                        val *= (mInvertMouseY) ? -1 : 1;
                    }

                    handleAxisInput(JoystickNum.KB, pair.Value, val);
                }
            }
        }

        /// <summary>
        /// Poll all input from all possible controllers (all buttons and axes for length of Unity's controller array)
        /// </summary>
        void getAllControllerInput()
        {
            if (!mIsInit)
                Start();

            for (int i = 1; i <= getJoystickNameArrLength(); i++)
            {
                if (mPlayerControllers[i] == null)
                    continue;

                //Poll and handle button input
                for (int j = 0; j <= 11; j++)
                {
                    if (Input.GetKeyDown("joystick " + i + " button " + j))
                        handleButtonInput((JoystickNum)i, "button " + j, KeyState.OnDown);

                    if (Input.GetKeyUp("joystick " + i + " button " + j))
                        handleButtonInput((JoystickNum)i, "button " + j, KeyState.OnUp);

                    if (Input.GetKey("joystick " + i + " button " + j))
                        handleButtonInput((JoystickNum)i, "button " + j, KeyState.IsDown);
                }

                //Poll and handle axis input
                float temp = Input.GetAxis("j" + i + " axis x");

                if (temp != 0f)
                {
                    handleAxisInput((JoystickNum)i, "axis x", temp);
                }

                temp = Input.GetAxis("j" + i + " axis y");
                if (temp != 0f)
                {
                    handleAxisInput((JoystickNum)i, "axis y", temp);
                }

                for (int j = 3; j <= 10; j++)
                {
                    temp = Input.GetAxis("j" + i + " axis " + j);
                    if (temp != 0.0f)
                        handleAxisInput((JoystickNum)i, "axis " + j, temp);
                    else if(mPlayerControllers[i] != null && mPlayerControllers[i].hasOnUpAxis())
                        handleAxisInput((JoystickNum)i, "axis " + j, 0.0f);

                }
            }
        }
    }
}