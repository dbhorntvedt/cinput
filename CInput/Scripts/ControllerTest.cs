﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CInput
{
    /// <summary>
    /// Test class for controller mappings
    /// </summary>
    public class ControllerTest : MonoBehaviour
    {

        private CManager man;

        void Awake()
        {
            man = transform.GetComponent<CManager>();
            //for (int i = 0; i < Input.GetJoystickNames().Length; i++)
            //{
            //    Debug.LogWarning(i + " " + Input.GetJoystickNames()[i]);
            //}
        }

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {


            if (Input.anyKey)
            {
                foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
                {
                    Debug.Log(kcode);
                }
            }

            for (int i = 1; i <= man.getJoystickNameArrLength(); i++)
            {
                for (int j = 0; j <= 11; j++)
                {
                    if (Input.GetKeyDown("joystick " + i + " button " + j))
                        Debug.Log("Button " + j + " on joystick " + i + " was pressed (controller type = " + man.getJoystickName((JoystickNum)i - 1));
                }

                float val = Input.GetAxis("j" + i + " axis x");
                if (val != 0f)
                    Debug.Log("Axis x on joystick " + i + " was triggered (controller type = " + man.getJoystickName((JoystickNum)i - 1) + " with value " + val);

                val = Input.GetAxis("j" + i + " axis y");
                if (val != 0f)
                    Debug.Log("Axis y on joystick " + i + " was triggered (controller type = " + man.getJoystickName((JoystickNum)i - 1) + " with value " + val);
                
                for (int j = 3; j <= 10; j++)
                {
                    val = Input.GetAxis("j" + i + " axis " + j);
                    if (val != 0.0f)
                        Debug.Log("Axis " + j + " on joystick " + i + " was triggered (controller type = " + man.getJoystickName((JoystickNum)i - 1) + " with value " + val);
                }
            }
        }
    }
}
