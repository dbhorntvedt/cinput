﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace CInput
{

    public enum ControllerScheme
    {
        INVALID_SCHEME = -1,
        PlayerControls,
    }

    public static class SetControllerScheme
    {
        /// <summary>
        /// Bind a scheme to player controller
        /// </summary>
        /// <param name="scheme">Controller scheme</param>
        /// <param name="playerController">Player controller component</param>
        public static void setScheme(ControllerScheme scheme, CPlayerControllerComponent playerController)
        {
            playerController.mController.unbindAll();
            schemes[scheme].Invoke(playerController);
        }
        
        /// <summary>
        /// Dictionary to associate scheme enum to a binding function
        /// </summary>
        private static readonly Dictionary<ControllerScheme, Action<CPlayerControllerComponent>> schemes = new Dictionary<ControllerScheme, Action<CPlayerControllerComponent>>()
        {
            {ControllerScheme.PlayerControls, setPlayerControls }
        };

        /// <summary>
        /// Function to bind multiple keys to functions as a scheme
        /// </summary>
        /// <param name="controller">Player controller component</param>
        private static void setPlayerControls(CPlayerControllerComponent controller)
        {
            controller.mController.bindButton(ControllerButton.A, KeyState.OnDown, controller.GetComponent<RigidbodyFirstPersonController>().jump);
            controller.mController.bindButton(ControllerButton.Left_Stick_Click, KeyState.OnDown, controller.GetComponent<RigidbodyFirstPersonController>().run);
            controller.mController.bindButton(ControllerButton.Y, KeyState.OnDown, controller.GetComponent<GrenadeHandler>().toggleThrowMethod);

            controller.mController.bindButton(ControllerButton.X, KeyState.OnDown, controller.gameObject.GetComponentInChildren<PushScript>().push);
            controller.mController.bindButton(ControllerButton.Right_Stick_Click, KeyState.OnDown, controller.gameObject.GetComponentInChildren<PushScript>().push);


            controller.mController.bindButton(ControllerButton.Right_Bumper, KeyState.OnDown, controller.GetComponent<GrenadeHandler>().cookSpec1);
            controller.mController.bindButton(ControllerButton.Left_Bumper, KeyState.OnDown, controller.GetComponent<GrenadeHandler>().cookSpec2);
            controller.mController.bindAxisAsButton(ControllerAxis.Left_Trigger, KeyState.OnDown, controller.GetComponent<GrenadeHandler>().cookMobility);
            controller.mController.bindAxisAsButton(ControllerAxis.Right_Trigger, KeyState.OnDown, controller.GetComponent<GrenadeHandler>().cookDamage);

            controller.mController.bindButton(ControllerButton.Right_Bumper, KeyState.OnUp, controller.GetComponent<GrenadeHandler>().throwSpec1);
            controller.mController.bindButton(ControllerButton.Left_Bumper, KeyState.OnUp, controller.GetComponent<GrenadeHandler>().throwSpec2);
            controller.mController.bindAxisAsButton(ControllerAxis.Left_Trigger, KeyState.OnUp, controller.GetComponent<GrenadeHandler>().throwMobility);
            controller.mController.bindAxisAsButton(ControllerAxis.Right_Trigger, KeyState.OnUp, controller.GetComponent<GrenadeHandler>().throwDamage);

            controller.mController.bindAxis(ControllerAxis.Left_Stick_X, controller.GetComponent<RigidbodyFirstPersonController>().setLastMoveX);
            controller.mController.bindAxis(ControllerAxis.Left_Stick_Y, controller.GetComponent<RigidbodyFirstPersonController>().setLastMoveY);
            controller.mController.bindAxis(ControllerAxis.Right_Stick_X, controller.GetComponent<RigidbodyFirstPersonController>().setLastLookX);
            controller.mController.bindAxis(ControllerAxis.Right_Stick_Y, controller.GetComponent<RigidbodyFirstPersonController>().setLastLookY);
        }
    }
}
