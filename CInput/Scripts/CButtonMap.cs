﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CInput
{
    /// <summary>
    /// All buttons on a controller (standardized to XBox buttons)
    /// </summary>
    public enum ControllerButton
    {
        INVALID_BUTTON = -1,
        A,
        B,
        X,
        Y,
        Start,
        Back,
        Left_Bumper,
        Right_Bumper,
        Left_Stick_Click,
        Right_Stick_Click,
        NUM_BUTTONS
    }

    /// <summary>
    /// Button action for CInput
    /// </summary>
    public class ButtonAction
    {
        private ControllerButton mButton { get; set; }

        private Dictionary<KeyState, Action> mDic = new Dictionary<KeyState, Action>();

        /// <summary>
        /// Button action constructor
        /// </summary>
        /// <param name="button">Controller button</param>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public ButtonAction(ControllerButton button, KeyState state, Action func)
        {
            mButton = button;
            mDic.Add(state, func);
        }

        /// <summary>
        /// Add action to button
        /// </summary>
        /// <param name="state">Button state</param>
        /// <param name="func">The function (IsDown, OnDown, OnUp)</param>
        public bool addAction(KeyState state, Action func)
        {
            if (!mDic.ContainsKey(state))
            {
                mDic.Add(state, func);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Rebind an action on button with specified state
        /// </summary>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public void rebindAction(KeyState state, Action func)
        {
            mDic[state] = func;
        }

        /// <summary>
        /// Unbind an action on button with specified state
        /// </summary>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        public void unbindState(KeyState state)
        {
            mDic.Remove(state);
        }

        /// <summary>
        /// Activate/invoke function
        /// </summary>
        public void activate(KeyState state)
        {
                if(mDic.ContainsKey(state)) mDic[state].Invoke();      
        }
    }

    /// <summary>
    /// Button mappings and parsing
    /// </summary>
    public class ButtonMap
    {
        public ControllerButton parseButtonInput(string button, ControllerType type)
        {
            switch (type)
            {
                case ControllerType.PlayStation3:
                    return PS3ButtonMap[button];
                case ControllerType.PlayStation4:
                    return PS4ButtonMap[button];
                case ControllerType.XBox360:
                    return XBox360ButtonMap[button];
                case ControllerType.XBox1:
                    return XBoxOneButtonMap[button];
                case ControllerType.Keyboard_Mouse:
                    return ControllerButton.INVALID_BUTTON;
                case ControllerType.UNKNOWN_CONTROLLER:
                    return ControllerButton.INVALID_BUTTON;     //Used when discovering controllers
                default:
                    throw new ArgumentOutOfRangeException("ControllerType", type, null);
            }
        }

        public readonly Dictionary<string, ControllerButton> PS3ButtonMap = new Dictionary<string, ControllerButton>()
        {
            {"button 0", ControllerButton.A},
            {"button 1", ControllerButton.B},
            {"button 2", ControllerButton.X},
            {"button 3", ControllerButton.Y},
            {"button 6", ControllerButton.Left_Bumper},
            {"button 7", ControllerButton.Right_Bumper},
            {"button 8", ControllerButton.Back},
            {"button 9", ControllerButton.Start},
            {"button 10",ControllerButton.Left_Stick_Click},
            {"button 11",ControllerButton.Right_Stick_Click},
        };

        public readonly Dictionary<string, ControllerButton> PS4ButtonMap = new Dictionary<string, ControllerButton>()
        {
            {"button 1", ControllerButton.A},
            {"button 2", ControllerButton.B},
            {"button 0", ControllerButton.X},
            {"button 3", ControllerButton.Y},
            {"button 4", ControllerButton.Left_Bumper},
            {"button 5", ControllerButton.Right_Bumper},
            {"button 8", ControllerButton.Back},
            {"button 9", ControllerButton.Start},
            {"button 10",ControllerButton.Left_Stick_Click},
            {"button 11",ControllerButton.Right_Stick_Click},
        };

        public readonly Dictionary<string, ControllerButton> XBoxOneButtonMap = new Dictionary<string, ControllerButton>()
        {
            {"button 0", ControllerButton.A},
            {"button 1", ControllerButton.B},
            {"button 2", ControllerButton.X},
            {"button 3", ControllerButton.Y},
            {"button 4", ControllerButton.Left_Bumper},
            {"button 5", ControllerButton.Right_Bumper},
            {"button 6", ControllerButton.Back},
            {"button 7", ControllerButton.Start},
            {"button 8", ControllerButton.Left_Stick_Click},
            {"button 9", ControllerButton.Right_Stick_Click}, 
        };

        public readonly Dictionary<string, ControllerButton> XBox360ButtonMap = new Dictionary<string, ControllerButton>()
        {
            {"button 0", ControllerButton.A},
            {"button 1", ControllerButton.B},
            {"button 2", ControllerButton.X},
            {"button 3", ControllerButton.Y},
            {"button 4", ControllerButton.Left_Bumper},
            {"button 5", ControllerButton.Right_Bumper},
            {"button 6", ControllerButton.Back},
            {"button 7", ControllerButton.Start},
            {"button 8", ControllerButton.Left_Stick_Click},
            {"button 9", ControllerButton.Right_Stick_Click},      
        };
    }
}