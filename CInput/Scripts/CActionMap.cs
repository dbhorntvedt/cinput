﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Debug = UnityEngine.Debug;

namespace CInput
{
    /// <summary>
    /// Action mappings for a single actor/player.
    /// </summary>
    public class CActionMap
    {
        private Dictionary<ControllerButton, ButtonAction> mButtonBinds;
        private Dictionary<ControllerAxis, AxisAction> mAxisBinds;
        private Dictionary<ControllerAxis, AxisAsButtonAction> mAxisToButtonBinds;

        //Value for press and release axis as button
        private float mAxisToButtonActivate;
        private float mAxisToButtonRelease;

        /// <summary>
        /// Constructor
        /// </summary>
        public CActionMap()
        {
            mButtonBinds = new Dictionary<ControllerButton, ButtonAction>();
            mAxisBinds = new Dictionary<ControllerAxis, AxisAction>();
            mAxisToButtonBinds = new Dictionary<ControllerAxis, AxisAsButtonAction>();
        }

        /// <summary>
        /// Set the values for AxisToButton
        /// </summary>
        public void setAxisToButtonVals(float activate, float release)
        {
            mAxisToButtonActivate = activate;
            mAxisToButtonRelease = release;
        }

        /// <summary>
        /// Bind a new button
        /// </summary>
        /// <param name="button">Controller button</param>
        /// <param name="buttonState">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public void bindButton(ControllerButton button, KeyState state, Action func)
        {
            if (!mButtonBinds.ContainsKey(button))
                mButtonBinds.Add(button, new ButtonAction(button, state, func));
            else if (!mButtonBinds[button].addAction(state, func))
                Debug.LogError("State " + state.ToString() + " is already bound to " + button.ToString() + "! Unbind first, or do a forced bind.");
        }

        /// <summary>
        /// Bind a new button. WILL REMOVE OLD BIND AUTOMATICALLY
        /// </summary>
        /// <param name="button">Controller button</param>
        /// <param name="buttonState">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public void bindButtonForced(ControllerButton button, KeyState state, Action func)
        {
            if (mButtonBinds.ContainsKey(button))
            {
                if (!mButtonBinds[button].addAction(state, func))
                {
                    unbindButtonState(button, state);
                    bindButton(button, state, func);
                }
            }
            else
                bindButton(button, state, func);
        }

        /// <summary>
        /// Unbind state from button
        /// </summary>
        /// <param name="button">Controller button</param>
        /// <param name="state">Button state (IsDown, OnDown, OnUp)</param>
        public void unbindButtonState(ControllerButton button, KeyState state)
        {
            if (mButtonBinds.ContainsKey(button)) mButtonBinds[button].unbindState(state);
        }

        /// <summary>
        /// Remove button bind
        /// </summary>
        /// <param name="button">Controller button</param>
        public void unbindButton(ControllerButton button)
        {
            if (mButtonBinds.ContainsKey(button)) mButtonBinds.Remove(button);
        }

        /// <summary>
        /// Unbind all buttons
        /// </summary>
        public void unbindAllButtons()
        {
            mButtonBinds.Clear();
        }

        /// <summary>
        /// Bind a new axis
        /// </summary>
        /// <param name="button">Controller axis</param>
        /// <param name="func">The function</param>
        public void bindAxis(ControllerAxis axis, Action<float> func)
        {
            if (!mAxisBinds.ContainsKey(axis) && !mAxisToButtonBinds.ContainsKey(axis))
                mAxisBinds.Add(axis, new AxisAction(axis, func));
            else
                Debug.LogError("Axis " + axis.ToString() + " is already bound. Please unbind key before binding, or make a forced bind.");
        }

        /// <summary>
        /// Bind a new axis. WILL REMOVE OLD BIND AUTOMATICALLY
        /// </summary>
        /// <param name="button">Controller button</param>
        /// <param name="buttonState">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public void bindAxisForced(ControllerAxis axis, Action<float> func)
        {
            unbindAxis(axis);
            bindAxis(axis, func);
        }

        /// <summary>
        /// Bind a new axis
        /// </summary>
        /// <param name="button">Controller axis</param>
        /// <param name="func">The function</param>
        public void bindAsButtonAxis(ControllerAxis axis, KeyState state, Action func)
        {
            if (mAxisBinds.ContainsKey(axis))
                Debug.LogError("Axis " + axis + " is already bound. Please unbind key before binding, or make a forced bind.");
            else if (!mAxisToButtonBinds.ContainsKey(axis))
                mAxisToButtonBinds.Add(axis, new AxisAsButtonAction(axis, state, func));
            else if (!mAxisToButtonBinds[axis].addAction(state, func))
                Debug.LogError("Axis " + axis + " with state " + state + " is already bound. Please unbind key before binding, or make a forced bind.");
        }

        /// <summary>
        /// Bind a new axis as button. WILL REMOVE OLD BIND AUTOMATICALLY
        /// </summary>
        /// <param name="axis">Controller axis to force as button</param>
        /// <param name="buttonState">Button state (IsDown, OnDown, OnUp)</param>
        /// <param name="func">The function</param>
        public void bindAxisAsButtonForced(ControllerAxis axis, KeyState state, Action func)
        {
            //unbindAxis(axis);
            if (!mAxisToButtonBinds.ContainsKey(axis))
                mAxisToButtonBinds.Add(axis, new AxisAsButtonAction(axis, state, func));
            else if (!mAxisToButtonBinds[axis].addAction(state, func))
                Debug.LogError("Could not force bind " + axis + " with state " + state + " for some reason");
        }

        /// <summary>
        /// Remove axis bind
        /// </summary>
        /// <param name="axis">Controller axis</param>
        public void unbindAxis(ControllerAxis axis)
        {
            if (mAxisBinds.ContainsKey(axis)) mAxisBinds.Remove(axis);
            else if (mAxisToButtonBinds.ContainsKey(axis)) mAxisToButtonBinds.Remove(axis);
        }

        /// <summary>
        /// Unbind all axes
        /// </summary>
        public void unbindAllAxes()
        {
            mAxisBinds.Clear();
            mAxisToButtonBinds.Clear();
        }

        /// <summary>
        /// Unbind ALL axes and buttons
        /// </summary>
        public void unbindAll()
        {
            unbindAllAxes();
            unbindAllButtons();
        }

        /// <summary>
        /// Checks if there are any bound AxisAsButton with OnUp
        /// </summary>
        public bool hasOnUpAxis()
        {
            foreach (KeyValuePair<ControllerAxis, AxisAsButtonAction> pair in mAxisToButtonBinds)
            {
                if (pair.Value.hasOnUp()) return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if an AxisAsButton has OnUp
        /// </summary>
        /// <param name="axis">Controller axis</param>
        public bool isOnUpAxis(ControllerAxis axis)
        {
            if (mAxisToButtonBinds.ContainsKey(axis))
                return mAxisToButtonBinds[axis].hasOnUp();

            return false;
        }

        /// <summary>
        /// Activate action bound to button
        /// </summary>
        /// <param name="button">Controller button</param>
        public void activateButtonFunc(ControllerButton button, KeyState state)
        {
            if (mButtonBinds.ContainsKey(button)) mButtonBinds[button].activate(state);
        }

        /// <summary>
        /// Activate action bound to axis
        /// </summary>
        /// <param name="axis">Controller axis</param>
        /// <param name="val">Analog input value</param>
        public void activateAxisFunc(ControllerAxis axis, float val)
        {
            if (mAxisBinds.ContainsKey(axis)) mAxisBinds[axis].activate(val);
            else if (mAxisToButtonBinds.ContainsKey(axis)) mAxisToButtonBinds[axis].activate(val, mAxisToButtonActivate, mAxisToButtonRelease);
        }

    }
}
